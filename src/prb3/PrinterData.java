package prb3;

import java.util.LinkedList;
import java.util.Queue;

public class PrinterData {

    Queue<String> queue;

    public PrinterData() {
        queue = new LinkedList<>();
    }

    public synchronized void produce(String document, int id) {
        while(hasDataToPrint()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Employee #" + id + " requested to print document: " + document);
        queue.add(document);
        notifyAll();
    }

    public synchronized String consume() {
        while(!hasDataToPrint()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while(hasDataToPrint()) {
            String document = queue.poll();
            System.out.println("Printing document: " + document);
            notifyAll();
            return document;
        }
        notifyAll();
        return null;
    }

    public boolean hasDataToPrint() {
        return !queue.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Printer Queue: ");
        String delimiter = " ";
        queue.stream().forEach(document -> builder.append(document + delimiter));

        return builder.toString();
    }
}
