package prb3;

import java.util.Random;

public class Employee extends Thread {
    private int id;
    private PrinterData data;
    private long timeToWriteDocument;

    public Employee(int id, PrinterData data, long timeToWriteDocument) {
        this.id = id;
        this.data = data;
        this.timeToWriteDocument = timeToWriteDocument;
    }

    @Override
    public void run() {
        int numberOfDocuments = new Random().nextInt(1) + 1;
        System.out.println("Employee #" + id + " will create and print " + numberOfDocuments + " documents; Time for a" +
                " document = " + timeToWriteDocument);
        for(int i=0;i<numberOfDocuments;i++) {
            try {
                sleep(timeToWriteDocument);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String document = "Document;Emp"+id+";Doc"+(i+1);
            System.out.println("Employee #" + id + " wants to print document #" + (i + 1));
            data.produce(document, id);
        }
    }
}
