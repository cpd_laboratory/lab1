package prb3;

public class Printer extends Thread {
    private long printingTime;
    private PrinterData queue;

    public Printer(long printingTime, PrinterData queue) {
        this.printingTime = printingTime;
        this.queue = queue;
    }

    @Override
    public void run() {
        while(1 == 1) {
            String document = queue.consume();
            try {
                Thread.sleep(printingTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Document " + document + " has been printed");
        }
    }
}
