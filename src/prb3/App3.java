package prb3;

import java.util.Random;

public class App3 {

    public static void main(String[] args) {
        int numberOfEmployees = 8;
        Employee[] employees = new Employee[numberOfEmployees];
        PrinterData printerQueue = new PrinterData();
        Random random = new Random();

        Printer printer = new Printer(1000L, printerQueue);

        for(int i=0;i<numberOfEmployees;i++) {
            employees[i] = new Employee(i+1,printerQueue,1000L + random.nextInt(3000));
        }

        printer.start();
        for(int i=0;i<numberOfEmployees;i++) {
            employees[i].start();
        }
    }
}
