package prb4;

public class ReaderConsumer extends Thread {
    private SharedMemory memory;

    public ReaderConsumer(SharedMemory memory){
        this.memory = memory;
    }

    @Override
    public void run() {
        while(!(memory.isFinished(true) && memory.isFinished(false))) {
            memory.readChar();
        }
    }
}
