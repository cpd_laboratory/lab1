package prb4;

public class ForwardThread extends Thread {
    private SharedMemory memory;

    public ForwardThread(SharedMemory memory) {
        this.memory = memory;
    }

    @Override
    public void run() {
        while(!isFinished()) {
            memory.request(true);
        }
    }

    public boolean isFinished() {
        return memory.isFinished(true);
    }
}
