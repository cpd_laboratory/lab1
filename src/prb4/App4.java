package prb4;

public class App4 {

    public static void main(String[] args) {
        SharedMemory memory = new SharedMemory("SomeRandomStringWrittenByMeToTestThisExercise");
        ForwardThread forwardThread = new ForwardThread(memory);
        BackwardThread backwardThread = new BackwardThread(memory);
        ReaderConsumer readerConsumer = new ReaderConsumer(memory);

        readerConsumer.start();
        forwardThread.start();
        backwardThread.start();
    }
}
