package prb4;

public class BackwardThread extends Thread {

    private SharedMemory memory;

    public BackwardThread(SharedMemory memory) {
        this.memory = memory;
    }

    @Override
    public void run() {
        while(!isFinished()) {
            memory.request(false);
        }
    }

    public boolean isFinished() {
        return memory.isFinished(false);
    }
}
