package prb4;

public class SharedMemory {
    private String data;
    private int forwardPointer;
    private int backwardPointer;
    private boolean reading;
    private boolean readingDirection;

    public SharedMemory(String data) {
        this.data = data;
        forwardPointer = -1;
        backwardPointer = data.length();
        reading = false;
        readingDirection = false;
    }

    public synchronized void request(boolean direction) {
        while(reading) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(direction) {
            reading = true;
            readingDirection = true;
            forwardPointer++;
        } else {
            reading = true;
            readingDirection = false;
            backwardPointer--;
        }
        notifyAll();

    }

    public synchronized void readChar() {
        while(!reading) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(readingDirection) {
            System.out.println("Forward reading at index " + forwardPointer + " with character " + data.charAt(forwardPointer));
        } else {
            System.out.println("Backward reading at index " + backwardPointer + " with character " + data.charAt(backwardPointer));
        }
        reading = false;
        notifyAll();
    }

    public boolean isFinished(boolean readingDirection) {
        //true - forward
        //false - backward
        if(readingDirection) {
            return forwardPointer >= data.length() - 1;
        } else {
            return backwardPointer <= 0;
        }
    }
}
